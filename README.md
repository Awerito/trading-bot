# Trading Bot

[API Token](https://www.alphavantage.co/support/#api-key)

## Bibliography

- [Kagle](https://www.kaggle.com/c/jane-street-market-prediction/discussion/199098)
    - [Using Machine Learning And Alternative Data To - Predict Movements In Market Risk](https://arxiv.org/abs/2009.07947)
