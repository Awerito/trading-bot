import json
import matplotlib.pyplot as plt
from secret import TOKEN
from alpha_vantage.timeseries import TimeSeries


STOCK = 'FB'
ts = TimeSeries(key=TOKEN, output_format='pandas')
data, meta_data = ts.get_daily(STOCK)

for col in data.columns[:-1]:
    data[col].plot()

plt.title(STOCK)
plt.xlabel("Date (daily)")
plt.ylabel("Value")
plt.grid(True)
plt.xticks(rotation=45)
plt.legend()
plt.show()
